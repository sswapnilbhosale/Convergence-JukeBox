package com.gslab.bean;

import java.util.Arrays;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("bookingBean")
public class BookingBean implements Comparable<BookingBean>{

	private int booked;
	private int slotNo;
	private int sessionId;
	public static String[] lunchSlotName = new String[]{"","1:00 - 1:05","1:05 - 1:10","1:10 - 1:15","1:15 - 1:20","1:20 - 1:25","1:25 - 1:30","1:30 - 1:35","1:35 - 1:40","1:40 - 1:45","1:45 - 1:50","1:50 - 1:55","1:55 - 2:00"};
	public static String[] snacksSlotName = new String[]{"","4:00 - 4:05","4:05 - 4:10","4:10 - 4:15","4:15 - 4:20","4:20 - 4:25","4:25 - 4:30","4:30 - 4:35","4:35 - 4:40","4:40 - 4:45","4:45 - 4:50","4:50 - 4:55","4:55 - 5:00"};
	private String slotName;
		
	
	@Autowired
	@Qualifier("songsBean")
	private SongsBean songBean;
	
	@Autowired
	@Qualifier("loginBean")
	private LoginBean loginBean;
	
	private Calendar date;
	
	
		public BookingBean(int isBooked, int slotNo, int sessionId,
			SongsBean songBean, LoginBean loginBean, Calendar date) {
		super();
		this.booked = isBooked;
		this.slotNo = slotNo;
		this.sessionId = sessionId;
		this.songBean = songBean;
		this.loginBean = loginBean;
		this.date = date;
	}


	public BookingBean() {

		booked = 0;
		slotNo = 0;
		sessionId = 0;
		songBean = null;
		loginBean = null;
		date = null;
		date = null;
		
	}
	
		
	public String getSlotName() {
		return slotName;
	}


	public void setSlotName(String slotName) {
		this.slotName = slotName;
	}


	public String[] getLunchSlotName() {
		return lunchSlotName;
	}


	public String[] getSnacksSlotName() {
		return snacksSlotName;
	}
	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	

	public int getBooked() {
		return booked;
	}


	public void setBooked(int booked) {
		this.booked = booked;
	}


	public int getSlotNo() {
		return slotNo;
	}
	public void setSlotNo(int slotNo) {
		this.slotNo = slotNo;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	
	
	public SongsBean getSongBean() {
		return songBean;
	}


	public void setSongBean(SongsBean songBean) {
		this.songBean = songBean;
	}


	
	public LoginBean getLoginBean() {
		return loginBean;
	}


	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}


	


	


	@Override
	public String toString() {
		return "BookingBean [booked=" + booked + ", slotNo=" + slotNo
				+ ", sessionId=" + sessionId + ", lunchSlotName="
				+ Arrays.toString(lunchSlotName) + ", snacksSlotName="
				+ Arrays.toString(snacksSlotName) + ", slotName=" + slotName
				+ ", songBean=" + songBean + ", loginBean=" + loginBean
				+ ", date=" + date + "]";
	}


	public int compareTo(BookingBean bean) {
		if(this.getSlotNo() > bean.getSlotNo()) {
			return 1;
		} else {
			return -1;
		}
			
	}
	
	
	
}
