package com.gslab.bean;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.stereotype.Component;

@Component("loginBean")
public class LoginBean {
	
	@NotNull(message="Enter Username")
	//@Pattern(regexp="^[G g][S s]\\-[0-9]{4}", message="Enter Valid Username...!!")
	private String username;
	
	@NotNull(message="Enter Password")
	//@Pattern(regexp="^.{8,50}", message="Enter Valid Password...!!")	
	private String password;
	
	private String role;
	private String fName;
	private String lName;

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginBean [username=" + username + ", password=" + password
				+ ", role=" + role + ", fName=" + fName + ", lName=" + lName
				+ "]";
	}

	public LoginBean() {
		// TODO Auto-generated constructor stub
		username = null;
		password = null;
		role = null;
		fName = null;
		lName = null;
	}

	public LoginBean(String username, String password, String role,
			String fName, String lName) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
		this.fName = fName;
		this.lName = lName;
	}

	

}
