package com.gslab.bean;

import org.springframework.stereotype.Component;


@Component("songsBean")
public class SongsBean {
	
	private int songId;
	private String path;
	private String name;
	private String artist;
	
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getSongId() {
		return songId;
	}
	public void setSongId(int songId) {
		this.songId = songId;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public SongsBean() {
		songId = 0;
		path = null;
		name = null;
		artist = null;
	}
	@Override
	public String toString() {
		return "SongsBean [songId=" + songId + ", path=" + path + ", name="
				+ name + ", artist=" + artist + "]";
	}
	public SongsBean(int songId, String path, String name, String artist) {
		super();
		this.songId = songId;
		this.path = path;
		this.name = name;
		this.artist = artist;
	}
	
	
	

}
