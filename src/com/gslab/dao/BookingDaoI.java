package com.gslab.dao;

import java.sql.Date;
import java.util.List;

import com.gslab.bean.BookingBean;
import com.gslab.bean.SongsBean;

public interface BookingDaoI {

	public abstract List<BookingBean> getAfterNoonSessionList(Date date);

	public abstract List<BookingBean> getEveningSessionList(Date date);
	
	public abstract boolean bookSongSlot(String username,int slotNo,int sessionId,int songId,Date currentDate);
	
	public List<SongsBean> search(String q);
	
	public List<SongsBean> getAllSongs();
	
	public abstract boolean getSlotStatus(int slot,int sessionId,Date date);

}