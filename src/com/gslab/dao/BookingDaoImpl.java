package com.gslab.dao;

import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.gslab.bean.BookingBean;
import com.gslab.bean.LoginBean;
import com.gslab.bean.SongsBean;
import com.gslab.mapper.BookingMapper;

@Repository("bookingDao")
public class BookingDaoImpl implements BookingDaoI {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = Logger.getLogger(BookingDaoImpl.class);

	public static String lunchSessionPath = "D://lunchSession/";
	public static String snacksSessionPath = "D://snacksSession/";

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gslab.dao.BookingDaoI#getAfterNoonSessionList(java.sql.Date)
	 */
	public List<BookingBean> getAfterNoonSessionList(Date date) {

		String query = "SELECT b.username,b.slotNo,b.sessionId,b.isBooked,b.date,s.songId,s.songName,s.songPath FROM bookings b,songs s,users u WHERE b.sessionId = 1 AND b.date = ? AND b.songId = s.songId AND b.username = u.username";
		Object[] obj = new Object[] { date };
		List<BookingBean> list = jdbcTemplate.query(query, obj,
				new BookingMapper());
		Collections.sort(list);
		logger.info("Returning Afternoon Session Data : " + list.toString());
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gslab.dao.BookingDaoI#getEveningSessionList(java.sql.Date)
	 */
	public List<BookingBean> getEveningSessionList(Date date) {
		// String query =
		// "SELECT * FROM bookings WHERE sessionId = 2 and date = ?";
		String query = "SELECT b.username,b.slotNo,b.sessionId,b.isBooked,b.date,s.songId,s.songName,s.songPath FROM bookings b,songs s,users u WHERE b.sessionId = 2 AND b.date = ? AND b.songId = s.songId AND b.username = u.username";
		Object[] obj = new Object[] { date };
		List<BookingBean> list = jdbcTemplate.query(query, obj,
				new BookingMapper());
		Collections.sort(list);
		logger.info("Returning Evening Session Data : " + list.toString());
		return list;
	}

	public boolean bookSongSlot(String username, int slotNo, int sessionId,
			int songId, Date currentDate) {
		String destPath = "";
		String query = "UPDATE bookings set username=?,songId=?, isBooked=1 WHERE slotNo=? AND sessionId=? AND date = ?";
		Object[] args = new Object[] { username, songId, slotNo, sessionId,
				currentDate };
		int rs = jdbcTemplate.update(query, args);
		if (rs == 1) {
			SongsBean bean = this.getSongDetails(songId);
			if (sessionId == 1) {
				destPath = lunchSessionPath;
			} else {
				destPath = snacksSessionPath;
			}
			if (bean != null) {
				boolean result = this.copyFile(bean.getPath(), destPath + ""
						+ slotNo + ".mp3");
				if (result)
					return true;
			}

		}
		return false;

	}

	public SongsBean getSongDetails(int songId) {
		String query = "SELECT * FROM songs WHERE songId = ?";
		Object[] args = new Object[] { songId };
		SongsBean bean = null;
		try {
			bean = jdbcTemplate.queryForObject(query, args,
					new RowMapper<SongsBean>() {

						public SongsBean mapRow(ResultSet rs, int arg1)
								throws SQLException {
							SongsBean bean = new SongsBean();
							bean.setSongId(rs.getInt(1));
							bean.setName(rs.getString(2));
							bean.setPath(rs.getString(3));
							return bean;
						}
					});
			return bean;

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	public boolean copyFile(String src, String dest) {
		Path from = Paths.get(src);
		Path to = Paths.get(dest);
		CopyOption[] option = new CopyOption[] {
				StandardCopyOption.REPLACE_EXISTING,
				StandardCopyOption.COPY_ATTRIBUTES };
		try {
			Files.copy(from, to, option);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<SongsBean> search(String q) {
		String query = "SELECT * FROM songs WHERE (songName LIKE ?) OR (artist LIKE ?)";
		System.out.println("Query Param : " + q);
		Object[] args = new Object[] { "%" + q + "%", "%" + q + "%" };
		List<SongsBean> bean = jdbcTemplate.query(query, args,
				new RowMapper<SongsBean>() {

					public SongsBean mapRow(ResultSet rs, int arg1)
							throws SQLException {
						SongsBean bean = new SongsBean();
						bean.setSongId(rs.getInt(1));
						bean.setName(rs.getString(2));
						bean.setPath(rs.getString(3));
						bean.setArtist(rs.getString(4));
						return bean;
					}
				});
		return bean;

	}

	public List<SongsBean> getAllSongs() {
		String query = "SELECT * FROM songs where songId > 0 ";
		List<SongsBean> bean = jdbcTemplate.query(query,
				new RowMapper<SongsBean>() {

					public SongsBean mapRow(ResultSet rs, int arg1)
							throws SQLException {
						SongsBean bean = new SongsBean();
						bean.setSongId(rs.getInt(1));
						bean.setName(rs.getString(2));
						bean.setPath(rs.getString(3));
						bean.setArtist(rs.getString(4));
						return bean;
					}
				});
		if (bean.size() > 0)
			return bean;
		return null;

	}

	public boolean getSlotStatus(int slot, int sessionId, Date date) {
		String query = "SELECT username FROM bookings WHERE slotNo=? AND sessionId=? AND date=?";
		Object[] obj = new Object[] { slot, sessionId, date };
		BookingBean bean = jdbcTemplate.queryForObject(query, obj,
				new RowMapper<BookingBean>() {

					public BookingBean mapRow(ResultSet rs, int arg1)
							throws SQLException {
						// TODO Auto-generated method stub
						LoginBean loginBean = new LoginBean();
						loginBean.setUsername(rs.getString(1));
						BookingBean bookingBean = new BookingBean();
						bookingBean.setLoginBean(loginBean);
						return bookingBean;
					}

				});
		if (bean.getLoginBean().getUsername().compareToIgnoreCase("admin") == 0) {
			return false;
		}
		return true;

	}

}
