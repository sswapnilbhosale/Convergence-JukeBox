package com.gslab.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.gslab.bean.BookingUserBean;
import com.gslab.bean.LoginBean;
import com.gslab.bean.SongsBean;
import com.gslab.mapper.LoginMapper;

@Repository("loginDao")
public class LoginDaoImpl implements LoginDaoI {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gslab.dao.LoginDaoI#validateUser(java.lang.String,
	 * java.lang.String)
	 */
	public LoginBean validateUser(String userName, String password) {
		String query = "SELECT * FROM users WHERE username = ? AND password = ?";
		Object[] obj = new Object[] { userName, password };
		return (LoginBean) jdbcTemplate.queryForObject(query, obj,
				new LoginMapper());
	}

	public BookingUserBean getBookingStatus(String username, Date date) {
		String query = "SELECT username,isLunchSlotBooked,isSnacksSlotBooked FROM booking_users WHERE username = ? AND date = ?";
		Object[] obj = new Object[] { username, date };
		BookingUserBean bean1 = null;
		try {
			bean1 = jdbcTemplate.queryForObject(query, obj,
					new RowMapper<BookingUserBean>() {

						public BookingUserBean mapRow(ResultSet rs, int arg1)
								throws SQLException {
							BookingUserBean bean = new BookingUserBean();
							LoginBean loginBean = new LoginBean();
							loginBean.setUsername(rs.getString(1));
							bean.setLoginBean(loginBean);
							bean.setIsLunchSlotBooked(rs.getString(2));
							bean.setIsSnacksSlotBooked(rs.getString(3));
							return bean;
						}
					});
			System.out.println("Booking Usewr Bean : " + bean1);
			if (bean1 == null) {
				return null;
			} else {
				return bean1;
			}
		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	public boolean updateUserBookings(String username, int sessionId,
			Date currentDate) {
		BookingUserBean bean = this.getBookingStatus(username, currentDate);
		if (bean == null) {
			String query = sessionId == 1 ? "INSERT INTO booking_users(username,isLunchSlotBooked,date) VALUES (?,'1',?)"
					: "INSERT INTO booking_users(username,isSnacksSlotBooked,date) VALUES (?,'1',?)";
			Object[] obj = new Object[] { username, currentDate };
			int result = jdbcTemplate.update(query, obj);
			if (result == 1)
				return true;
			return false;
		} else {
			String query = sessionId == 1 ? "UPDATE booking_users set isLunchSlotBooked='1' WHERE username=? AND date = ?"
					: "UPDATE booking_users set isSnacksSlotBooked='1' WHERE username=? AND date = ?";
			Object[] args = new Object[] { username, currentDate };
			int rs = jdbcTemplate.update(query, args);
			if (rs == 1)
				return true;
			return false;
		}

	}
}
