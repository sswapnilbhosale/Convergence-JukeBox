package com.gslab.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class JukeBoxInterceptor implements HandlerInterceptor {

	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	public void postHandle(HttpServletRequest req, HttpServletResponse res,
			Object obj, ModelAndView model) throws Exception {
		// TODO Auto-generated method stub
		
		

	}

	public boolean preHandle(HttpServletRequest req, HttpServletResponse res,
			Object obj) throws Exception {
		// TODO Auto-generated method stub
		String url = req.getRequestURI();
		if(!url.endsWith("index.html") && !url.endsWith("login.html")) {
			System.out.println("Inside checking");
			if(req.getSession().getAttribute("userInfo") == null) {
				res.sendRedirect("index.html");	
				return false;
			}
		}
		return true;
	}

}
