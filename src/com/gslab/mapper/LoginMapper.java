package com.gslab.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gslab.bean.LoginBean;

public class LoginMapper implements RowMapper<LoginBean>{

	public LoginBean mapRow(ResultSet rs, int arg1) throws SQLException {
		
		LoginBean bean = new LoginBean();
		bean.setUsername(rs.getString(1));
		bean.setPassword(rs.getString(2));
		bean.setfName(rs.getString(3));
		bean.setlName(rs.getString(4));
		bean.setRole(rs.getString(5));
		return bean;
	}
	
}
