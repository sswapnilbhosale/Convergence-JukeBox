package com.gslab.service;

import java.sql.Date;
import java.util.List;

import com.gslab.bean.BookingBean;

public interface BookingServiceI {

	public abstract List<BookingBean> getAfternoonSessionSongs(Date date);

	public abstract List<BookingBean> getEveningSessionSongs(Date date);
	
	public abstract java.util.Date getToday();
	
	public abstract boolean bookSongSlot(String username,int slotNo,int sessionId,int songId);
	
	public abstract List searchSong(String query);
	
	public abstract List getSongs();
	
	public abstract boolean getSlotStatus(int slot,int sessionId,Date date);
	

}