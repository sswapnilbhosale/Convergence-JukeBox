package com.gslab.service;

import java.sql.Date;

import com.gslab.bean.BookingUserBean;
import com.gslab.bean.LoginBean;

public interface LoginServiceI {

	public abstract LoginBean checkUser(String username, String password);
	
	public abstract BookingUserBean getBookingStatus(String userName,Date date);
	public abstract boolean updateUserBookings(String username, int sessionId,Date currentDate);

}